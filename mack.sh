#! /bin/bash
##=====================================================================##
## PROGRAM........................................................MACK ##
## AUTH.............................................................ET ##
## DISCRIPT......................A SIMPLE SCRIPT FOR USEING MACCHANGER ##
##=====================================================================##
## A FEW FUNCTIONS
down(){
ip link set dev $iface down
}
up(){
ip link set dev $iface up
}
## THE OPTIONS
iopt=($(ip token | sed s/"token :: dev"//))
macopt=(
keep-mac
keep-vendor
keep-type
any-type
full-random
set-mac
reset-mac
)
errormsg=(
"NOT ROOT RUN AGAIN AS ROOT"
"MACCHANGER NOT INSTALLED GO INSTAL IT"
"YOU NEED IP FOR THIS"
"NO INTERFACE THERE MUST BE ONE ATLEAST IDIOT"
)
msg=(
"root....................OK"
"macchanger..............OK"
"ip......................OK"

)
## CHECK ON THINGS
if [ $UID -ne 0 ]; then
  echo ${errormsg[0]}
  exit 1
else
  printf "%s \n"  "${msg[0]}"
fi
if hash macchanger ; then
  printf "%s \n" "${msg[1]}"
else
  echo ${errormsg[1]}
  exit 94
fi
if hash ip ; then
  printf "%s \n \n" "${msg[2]}"
else
  echo ${errormsg[2]}
  exit 16
fi
## SELECT AN INLTERFACE
if [ ${#iopt[@]} -lt 1 ]; then
  echo ${errormsg[3]}
  exit 44
else
select iface in ${iopt[@]} ; do
  if [ "$iface" ]; then
    break
  fi
done
fi
## SHOW MAC
macchanger -s $iface
## SELECT AND EXICUTE MACCHANGER OPTIONS
select macchoice in ${macopt[@]}; do
  case $macchoice in
   ${macopt[0]})
        echo "bye then"
        break
    ;;
   ${macopt[1]})
        down
        macchanger -e $iface
        up
        break
    ;;
    ${macopt[2]})
        down
        macchanger -a $iface
        up
        break
    ;;
    ${macopt[3]})
        down
        macchanger -A $iface
        up
        break
    ;;
    ${macopt[4]})
        down
        macchanger -r $iface
        up
        break
    ;;
   ${macopt[5]})
        echo "enter new mac adress"
        read -N 17 newmac
        down
        macchanger -m $newmac $iface
        up
        break
    ;;
    ${macopt[6]})
        down
        macchanger -p $iface
        up
        break
    ;;
    *)
        echo "press the number next to your choice"
    ;;
  esac
done



